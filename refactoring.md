# Refactoring

Refactoring is the process of restructuring existing code without changing its external behavior. It aims to improve the code's internal structure, making it more readable, maintainable, and efficient.

## When refactoring is successful

- No Change in Functionality
- Improved Code Quality
    - Readability
    - Maintainability
- Enhanced Performance
    - Efficiency: The refactored code should not introduce performance regressions. In some cases, refactoring may even improve performance by optimizing algorithms or removing unnecessary computations.
- Reduced Technical Debt
    - Code Smells: The refactored code should address and eliminate existing code smells (e.g., duplicated code, long methods, large classes).
    - Simpler Design: The design should be simplified, adhering more closely to design principles such as SOLID, DRY, and KISS.
- Improved Test Coverage
- Easier Onboarding
- Predefined metrics are better (if you have any business/technical metrics)

## How to prepare for refactoring

- Understand the project/business.
- Write missing tests.
    - Feel safe to do changes.
- Apply code formatting before.
- Determine goals - what are you going to achieve? How the code should look like?
- Small step, many commits. Identify parts which have the least dependencies.
- Other remarks
    - Conventions sometimes break code, three ifs? I will add another one and I don't create a new class with dedicated logic because there is `if` convention.
    - Interfaces are good for everything - use DI, IOC
    - See if it can't be removed instead of trying to fix it
    - Continuous refactoring, we don't ask about it, it's part of my profession, do small fragments.
    - For business, refactor is not a value. It's hard to convince business. Although there is value for developers.
    - Use `feature switch` to switch between old and new implementations just in case.

## What does a refactoring usually do?

- Increase consistency by applying naming conventions etc.
- Reduce coupling and increase cohesion
- Force following good programming rules: SRP, IDP etc. and design patterns. Thanks to them the code is much more readable and expressive and easy to understand.
- Modularize mechanisms
- Shorten functions and classes
- Create more expressive names for functions/classes
- Reduce number of classes and methods
- Remove repeatings

## What does it mean that a function does one thing?

- It means that even we have many methods in the function, all of them are on the same abstract level (they want to achieve the same goal which is the main result of the function and do things on the same level for example append sth to the structure instead of creating objects, map them and then append them as a part of the structure) and you are not able to extract a new self-dependent function.
- This principle is often expressed through the idea that a function should have a single, well-defined purpose or responsibility.
  - A function should perform only one task or responsibility. This means it should not try to handle multiple operations or manage different concerns.
  - Example: A function that calculates the area of a rectangle should only calculate and return the area, not handle input validation or output formatting.
