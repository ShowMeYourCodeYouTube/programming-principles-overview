# Programming principles & best practises

A programming principle is a fundamental guideline or rule that developers follow to write clear, maintainable, efficient, and robust code. These principles help ensure that software systems are easier to understand, develop, and maintain over time. They serve as best practices that can improve the overall quality of the software and facilitate collaboration among developers.

Programming principles offer broad, enduring guidance on software design and architecture, while best practices provide specific, actionable recommendations tailored to particular contexts and technologies.

---

Types of Programming Principles
- Design Principles
  - These principles guide the structure and organization of code.
  - SOLID Principles:
      - S - Single Responsibility Principle (SRP): A class should have only one reason to change, meaning it should only have one job or responsibility. For example, instead of a single class handling data retrieval, report formatting, and printing, separate these tasks into distinct classes: DataRetriever, ReportFormatter, and ReportPrinter.
      - O - Open/Closed Principle (OCP): A class should be open for extension but closed for modification.
          - The Open/closed principle implies that you can create systems in which new features are added by adding new code as opposed to changing old code. In order to be perfectly conform to the open/closed principle one must have perfect foresight. For in order to create a system that is fully open to extension and closed to all modification one must be able to perfectly predict the future. One must know in advance what new features the customer will request in order to put extension points to the code.
          - It means that adding a new feature does not require changing your code but adding a new class/function so basically you extend the existing system.
            - What does it mean that OCP is broken?
              - When OCP is broken, it means that to extend functionality, you must modify existing code rather than extending it. This can lead to increased risk of bugs, reduced maintainability, and tight coupling.
              - It means that by adding a new type, you need to change the function.
          - [Reference](https://stackoverflow.com/questions/35658129/does-the-open-for-extension-closed-for-modification-principle-make-any-sense)
      - L - Liskov Substitution Principle (LSP): Objects of a superclass should be replaceable with objects of a subclass without affecting the functionality.
      - I - Interface Segregation Principle (ISP): Clients should not be forced to depend on interfaces they do not use.
      - D - Dependency Inversion Principle (DIP): High-level modules should not depend on low-level modules; both should depend on abstractions.
      - <https://blog.ndepend.com/layered-architecture-solid-approach/>
      - <https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design#open-closed-principle>
- Code Quality Principles
  - These principles focus on writing readable, understandable, and maintainable code.
  - DRY (Don't Repeat Yourself)
    - Avoid code duplication. If logic is repeated, abstract it into a reusable function or method.
  - KISS (Keep It Simple, Stupid)
    - Keep your code simple and avoid overcomplicating solutions.
    - Example: If a function can be implemented in a straightforward way, avoid adding extra layers or features that are not essential.
  - YAGNI (You Aren't Gonna Need It)
    - Explanation: Don’t add functionality until it is necessary.
    - Example: Don’t write methods or classes that aren’t currently needed for your application.
- Efficiency and Optimization Principles
    - These principles focus on writing efficient code that is optimized for performance.
        - Premature Optimization: Avoid optimizing code too early; focus on functionality first, then optimize for performance when necessary.
        - Time Complexity & Space Complexity: Understand the algorithmic efficiency (Big O notation) for ensuring scalability and resource utilization.
- Testing Principles
    - These principles help ensure code correctness through testing.
        - TDD (Test-Driven Development): Write tests before writing the code to ensure that the code meets the defined requirements.
        - Pyramid Testing: Structure your tests with unit tests at the base, followed by integration tests, and end-to-end tests on top.
        - Code Coverage: Aim for high test coverage, but don't sacrifice readability or maintainability for 100% coverage.
- Refactoring Principles
    - These principles guide improving the internal structure of code without changing its external behavior.
    - Code Smells: Watch for signs of code that may need refactoring, such as long methods, large classes, or complex conditionals.
    - Boy Scout Rule: Always leave the codebase cleaner than you found it (e.g., improving readability or removing unused code).
- Security Principles
    - These principles focus on writing secure software.
    - Principle of Least Privilege: Give components, functions, or users the least amount of privilege necessary to perform their tasks.
    - Input Validation: Always validate and sanitize user inputs to prevent common vulnerabilities like SQL injection or XSS (Cross-Site Scripting).

---

Other principles:
- All or some of OOP concepts
    - Encapsulation: Keeps an object's state private and provides methods to access and modify it.
    - Abstraction: Hides complex implementation details and exposes only the necessary parts of an object.
    - Modularity: Breaks down a program into smaller, interchangeable modules.
    - Test-Driven Development (TDD): Writing tests before code to define the desired behavior of the software.
- Separation of Concerns (SoC)
   - Explanation: Different concerns or aspects of a program should be separated into distinct sections, modules, or layers.
   - Example: Separating the user interface logic from business logic and data access logic.
- Single Level of Abstraction Principle (SLAP)
   - It's a guideline used to improve the readability and maintainability of code. It suggests that each method or function should operate at a single level of abstraction. This means that within a given method, all the statements should be at the same level of detail.
   - Example
     - To adhere to the Single Level of Abstraction Principle, the publishPost method should focus solely on high-level operations, such as checking permissions and orchestrating the publishing process. Low-level operations, like database interaction and notification sending, should be delegated to separate methods. 
     - By separating concerns, the publishPost method becomes easier to understand and maintain. It can now be considered a high-level orchestrator of the blog post publishing process, while the low-level details are encapsulated in other methods, adhering to the SLAP principle.
- [General Responsibility Assignment Software Patterns (GRASP)](#grasp)

## Law of Demeter (LoD)

The Law of Demeter states that a method of an object should only call methods of:
- The object itself.
- Its own fields.
- Objects passed as parameters.
- Objects it creates.
- Its direct components.

In simpler terms, an object should avoid invoking methods of objects returned by other methods (sometimes summarized as "only talk to your immediate friends").

Purpose
- Reduce Coupling: LoD aims to reduce the coupling between components, making the codebase more modular and easier to maintain.
- Enhance Maintainability: By minimizing dependencies, changes in one part of the system are less likely to affect other parts, making the system more robust to changes.
- Improve Readability: Code that follows LoD tends to be easier to understand because it restricts the interaction paths between objects.

### Example

```java
class Engine {
    public Oil getOil() { ... }
}

class Car {
    private Engine engine;

    public Engine getEngine() { return engine; }
}

class Driver {
    private Car car;

    public void checkOil() {
        // Violates the Law of Demeter
        Oil oil = car.getEngine().getOil();
        // Do something with the oil
    }
}
```

Here, Driver interacts with Oil through Car and Engine, which are intermediary objects. To adhere to LoD, you can refactor the code as follows:

```java
class Engine {
    public Oil getOil() { ... }
}

class Car {
    private Engine engine;

    public Oil getCarOil() {
        return engine.getOil();
    }
}

class Driver {
    private Car car;

    public void checkOil() {
        // Adheres to the Law of Demeter
        Oil oil = car.getCarOil();
        // Do something with the oil
    }
}
```

## GRASP

GRASP (General Responsibility Assignment Software Patterns) is a set of guidelines that helps designers make informed decisions during the object-oriented design process.

9 GRASP patterns:
- Creator
  - The Creator principle guides the allocation of responsibility for creating objects. According to this principle, a class should be responsible for creating objects of other classes if the first class aggregates, contains, or has a composition relationship with the second class. This principle promotes low coupling between classes, enabling better maintainability and reusability.
- Information Expert
  - The Information Expert principle focuses on assigning responsibilities to classes that possess the most information required to fulfill them. A class should be responsible for a particular task if it has the necessary information to perform that task effectively. By adhering to this principle, we can design systems where responsibilities are distributed efficiently among classes, improving cohesion and reducing dependencies.
- Low Coupling
  - The Low Coupling principle emphasizes designing classes with minimal dependencies on other classes.
- Controller
  - The Controller principle suggests that an entity responsible for handling a system operation should be a separate class. Controllers act as intermediaries between the user interface and the business logic, orchestrating the flow of information and managing the interaction between objects.
- High Cohesion
  - High Cohesion is a principle that advocates designing classes with a clear and focused purpose. A class should have a single responsibility and encapsulate related behaviors and data. Cohesive classes are easier to understand, test, and maintain.
  - Benefits: Easily understandable and maintainable, Code reuse, Low coupling
- Indirection
  - Indirection introduces an intermediate unit to communicate between the other units, so that the other units are not directly coupled.
  - Benefits: low coupling
  - Example: Adapter, Facade, Observer
- Polymorphism 
  - Polymorphism is a fundamental principle in object-oriented design that enables objects of different classes to be treated uniformly through a common interface. By leveraging polymorphism, we can design systems that are extensible and adaptable to new requirements. Polymorphic behavior allows for code reuse and promotes loosely coupled systems.
  - Example: polymorphism, data encapsulation, interfaces
- Protected Variations
  - It provides a well defined interface so that the there will be no affect on other units.
  - Example: polymorphism, data encapsulation, interfaces
- Pure Fabrication
  - The Pure Fabrication principle suggests the creation of artificial classes to improve system design. These classes are not directly tied to the domain or problem being solved but serve as helpers or connectors between other classes. Pure Fabrication aids in achieving low coupling and high cohesion by encapsulating complex operations or providing an interface to external systems.
  - Examples: Adapter, Strategy
  - Benefits: High cohesion, low coupling and can reuse this class.

References:
- https://patrickkarsh.medium.com/object-oriented-design-with-grasp-principles-8049fa63e52
- https://home.cs.colorado.edu/~kena/classes/5448/f12/presentation-materials/rao.pdf
  - Here you cna find nice diagrams showing each of the principles.

## Separation of Concerns (SoC)

Separation of Concerns (SoC) is a fundamental design principle in software engineering that suggests dividing a program into distinct sections, where each section (or "concern") addresses a specific aspect or functionality of the system. The goal is to reduce complexity and increase maintainability by organizing the system in a way that each part has a well-defined role and does not overlap with others.

In simpler terms, SoC means that each part of a system should be responsible for a single, distinct task, which leads to cleaner, more modular, and easier-to-understand code.

Examples:
- Layered Architecture: A common architectural style in software development is the layered architecture. In this approach, the system is divided into different layers, each responsible for a specific concern.
    - Presentation Layer: Handles user interfaces and interactions.
    - Business Logic Layer: Handles the core business functionality.
    - Data Access Layer: Manages database interactions.
    - Infrastructure Layer: Handles external systems and services, such as logging or messaging.
    - Each layer focuses on a particular aspect of the application, and interactions between layers are clearly defined, reducing interdependencies.
- Microservices Architecture: In a microservices architecture, each service is responsible for a specific domain or business function. For example, an online store might have separate services for user management, inventory, and orders. Each service has its own distinct concern and can be developed, deployed, and scaled independently of others.
    - The separation of concerns in microservices also extends to deployment. Since each service can be managed independently, you can scale them as needed and avoid unnecessary dependencies between them.

## Composition Over Inheritance

The Composition Over Inheritance rule is a key design principle in object-oriented programming (OOP). It encourages using composition (building objects by combining simpler components) rather than relying heavily on inheritance (where one class extends another) to achieve code reuse and maintainability.

## Dependency Injection vs Inversion of Control

- Both Dependency Injection (DI) and Inversion of Control (IoC) are design principles used in software development to improve modularity, testability, and maintainability.
- Inversion of Control is a general principle where the control flow of a program is managed by a framework or container rather than the program itself.
- Dependency Injection is a specific implementation of IoC that focuses on injecting dependencies into components to decouple them and improve modularity.
  - Inversion of Control is a principle in software engineering by which the control of objects or portions of a program is transferred to a container or framework. It's most often used in the context of object-oriented programming.
- Implementation:
  - IoC: Implemented through frameworks that manage application flow (e.g., event handling frameworks).
  - DI: Implemented through mechanisms for injecting dependencies into components (e.g., constructors, setters).

## Others

- https://github.com/webpro/programming-principles
