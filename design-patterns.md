# Design patterns

Design Patterns are typical solutions to commonly occurring problems in software design.

References
- https://refactoring.guru/design-patterns
- http://www.blackwasp.co.uk/gofpatterns.aspx

What are levels of software development in a context of principles/rules etc?
- Ref: <https://stackoverflow.com/questions/4270912/what-is-the-difference-between-gof-and-grasp-design-patterns>
- Top Level
    - At the top level, tutorials present Main Principles. (based on Object-Oriented Analysis and Design with Applications, Grady Booch, et al. 1991)
    - Object Oriented Main Principles
        - Abstraction
        - Encapsulation (Information Hiding)
        - Hierarchy (Inheritance, Aggregation)
        - Modularity (High Cohesion and Loosely Coupling)
    - All subsequent principles and patterns is related to this main principles and try to support and improve them.
- Second Level
    - In the second level of resource there are Principles and trying to support and improve Main Principles of Object Orientation.
        - SOLID Principles
        - GRASP Principles
        - Law of Demeter-LoD
        - and etc. (other single named principles like LoD)
- Third Level
    - This level of resources called Design Patters. Gof (Gang of Four) have the first book about Design Patterns. There is 23 Design Patterns to support main principles and other principles of Object Oriented Design.
    - After GoF, many books written about using Design Patterns in specific programming language and specific frameworks (e.g. J2EE Deign Patterns, JQuery Design Patterns and etc.)
    - Design Patterns are more detailed and so close to programming language. However, Main Principles (at Top Level) and Principles (at Second Level) is so abstract.
- Forth Level
    - In this level, resources called : Heuristics.
    - The resource of this level are so detailed and very specific than other groups.
    - The best resource is Object Oriented Design Heuristics (72 Heuristics) , Book by Arthur J. Riel, 1996
- Fifth Level
    - At the final level, there are Guideline.
    - All Do It, Don't Do it, Avoid, ... guidelines in the books can be gather in this group.


## GOF design patterns

- The GoF Design Patterns are broken into three categories: Creational Patterns for the creation of objects; Structural Patterns to provide relationship between objects; and finally, Behavioral Patterns to help define how objects interact.
- The Gang of Four are the four authors of the book, "Design Patterns: Elements of Reusable Object-Oriented Software". In this article their twenty-three design patterns are described with links to UML diagrams, source code and real-world examples for each.

### Creational Design Patterns

These design patterns provide ways to create objects while hiding the creation logic, instead of instantiating objects directly using the new operator. This gives the program more flexibility in deciding which objects need to be created for a given use case.

- Abstract Factory. Allows the creation of objects without specifying their concrete type.
- Builder. Uses to create complex objects.
- Factory Method. Creates objects without specifying the exact class to create.
- Prototype. Creates a new object from an existing object.
- Singleton. Ensures only one instance of an object is created.

### Structural Design Patterns

Structural Patterns: These design patterns deal with class and object composition. The concept of inheritance is used to compose interfaces and define ways to compose objects to obtain new functionality.

- Adapter. Allows for two incompatible classes to work together by wrapping an interface around one of the existing classes.
- Bridge. Decouples an abstraction so two classes can vary independently.
- Composite. Takes a group of objects into a single object.
- Decorator. Allows for an object’s behavior to be extended dynamically at run time.
- Facade. Provides a simple interface to a more complex underlying object.
- Flyweight. Reduces the cost of complex object models.
- Proxy. Provides a placeholder interface to an underlying object to control access, reduce cost, or reduce complexity.

### Behavior Design Patterns

- Chain of Responsibility. Delegates commands to a chain of processing objects.
- Command. Creates objects which encapsulate actions and parameters.
- Interpreter. Implements a specialized language.
- Iterator. Accesses the elements of an object sequentially without exposing its underlying representation.
- Mediator. Allows loose coupling between classes by being the only class that has detailed knowledge of their methods.
- Memento. Provides the ability to restore an object to its previous state.
- Observer. Is a publish/subscribe pattern which allows a number of observer objects to see an event.
- State. Allows an object to alter its behavior when its internal state changes.
- Strategy. Allows one of a family of algorithms to be selected on-the-fly at run-time.
- Template Method. Defines the skeleton of an algorithm as an abstract class, allowing its sub-classes to provide concrete behavior.
- Visitor. Separates an algorithm from an object structure by moving the hierarchy of methods into one object.

Ref: 
- <https://springframework.guru/gang-of-four-design-patterns/>
- <http://www.blackwasp.co.uk/gofpatterns.aspx>

## Miscellanea

- Why can Singleton be considered as anti-pattern?
    - Ref: <https://medium.com/@cancerian0684/singleton-design-pattern-and-how-to-make-it-thread-safe-b207c0e7e368>
    - Singleton is mostly considered an anti-Pattern, because it brings inherent complexity to the system in terms of testing. Only DI frameworks (Spring, Dagger, etc) should be allowed to create Singletons for you rather than you writing the singletons. However, if it is absolute essential to write your own Singleton, then you must write it correctly.
    - The problem with single threaded version is that it will not work predictably when multiple threads tries to access singleton. A lock must be obtained in case two or more threads call getHelper() simultaneously, otherwise there may be problems:
        - Both threads may try to create the object at the same time, leading to two objects.
        - One thread may end up getting a reference to an incompletely initialized object.
